#include <stdio.h>
#include <stdlib.h>

void menu();
void encabezado();
void creditos();

int votar();
int validacion();
void datos(char nombre [][35], int identificacion [], char rol [][15], int  voto[], int cantidad, int i, int votoCandidato1, int votoCandidato2, int votoCandidato3);

int main()
{
   int opt;
   do
   {
        menu();
        scanf("%c",&opt);
        getchar();
        switch (opt)
            {
            case '1':
                encabezado("Unidad 1");
                printf("\n\n");
                ejercicio1();
                getchar();
                system("cls");
            break;

            case '2':
                encabezado("Unidad 2");
                printf("\n\n");
                ejercicio2();
                getchar();
                system("cls");
            break;

            case '3':
                encabezado("Unidad 4");
                printf("\n\n");
                ejercicio3();
                getchar();
                system("cls");
            break;

            case '4':
                printf("\n\n");
                creditos();
                getchar();
                system("cls");
            break;

            default:
                 printf("\nPrograma Finalizado");
            break;
            }
   } while (opt != '5');

return 0;
}
void menu(){
        printf("|------------------------------|\n");
        printf("|------------------------------|\n");
        printf("|---    Actividad Final   -----|\n");
        printf("|------------------------------|\n");
        printf("|-1. Ejercicio Unidad 1      --|\n");
        printf("|-2. Ejercicio Unidad 2      --|\n");
        printf("|-3. Ejercicio Unidad 4      --|\n");
        printf("|-4. Creditos                --|\n");
        printf("|-5. Salir                   --|\n");
        printf("|------------------------------|\n");
        printf("|-Ingrese una opcion:         -|\n");
        printf("|------------------------------|\n");
}
void encabezado(char texto[]){

        printf("|------------------------------|\n");
        printf("|---   Ejercicio %s   ---|",texto);
}
void creditos(){
        printf("|------------------------------|\n");
        printf("|------------------------------|\n");
        printf("|----       Creditos      -----|\n");
        printf("|------------------------------|\n");
        printf("|--                          --|\n");
        printf("|--                          --|\n");
        printf("|--      Grupo 243002_38     --|\n");
        printf("|--       Programacion       --|\n");
        printf("|--          ------          --|\n");
        printf("|------------------------------|\n");
        printf("|------------------------------|\n");
        printf("|--   Enter para continuar   --|\n");
        printf("|------------------------------|\n");
        printf("|------------------------------|\n");
}
void ejercicio1(){
char membresia;
    int noches;
    int valor_noche = 85000;
    int total_noche;
    int porcentaje;
    int descuento;
    printf("Seleccione el tipo de membresia: A,B,C\n");
    scanf("%s", &membresia);
    if (membresia == 'A' || membresia == 'a')
    {
        printf("Membresia tipo A\n");
        printf("Ingrese cantidad noches de hospedaje:\n");
        scanf("%i", &noches);
        total_noche = noches * valor_noche;
        printf("Valor de noches: %i\n", total_noche);

        if(noches > 4){
            printf("Tienes un descuento del 25 porciento:\n");
            porcentaje = total_noche * 0.25;
            descuento = total_noche - porcentaje;
            printf("Total a pagar: %i", descuento);

        }else{
            printf("Tienes un descuento del 15 porciento\n");
            porcentaje = total_noche * 0.15;
            descuento = total_noche - porcentaje;
            printf("Total a pagar: %i", descuento);
        }
    }
     else if (membresia == 'B' || membresia == 'b')
    {
        printf("Membresia tipo B\n");
        printf("Ingrese cantidad noches de hospedaje:\n");
        scanf("%i", &noches);
        total_noche = noches * valor_noche;
        printf("Valor de noches: %i\n", total_noche);

        if(noches > 3){
            printf("Tienes un descuento del 15 porciento:\n");
            porcentaje = total_noche * 0.15;
            descuento = total_noche - porcentaje;
            printf("Total a pagar: %i", descuento);

        }else{
            printf("Tienes un descuento del 10 porciento\n");
            porcentaje = total_noche * 0.10;
            descuento = total_noche - porcentaje;
            printf("Total a pagar: %i", descuento);
        }
    }
    else if (membresia == 'C' || membresia == 'c')
    {
        printf("Membresia tipo C\n");
        printf("Ingrese cantidad noches de hospedaje:\n");
        scanf("%i", &noches);
        total_noche = noches * valor_noche;
        printf("Valor de noches: %i\n", total_noche);

        if(noches > 2){
            printf("Tienes un descuento del 10 porciento:\n");
            porcentaje = total_noche * 0.10;
            descuento = total_noche - porcentaje;
            printf("Total a pagar: %i", descuento);

        }else{
            printf("Tienes un descuento del 5 porciento\n");
            porcentaje = total_noche * 0.05;
            descuento = total_noche - porcentaje;
            printf("Total a pagar: %i", descuento);
        }
    }else{
        printf("NO existe menbresia\n");
    }
    getchar();
    printf("\n\nPresione una tecla para continuar...\n");
    return 0;
}
void ejercicio2(){
char producto;
    char opcion;
    char categoria;
    int precio;
    int totalA;
    int contadorA;
    int totalB;
    int contadorB;
    int totalC;
    int contadorC;
    int totalglobal;
    int totalCantidad;
    int i;

    totalA = 0;
    contadorA = 0;
    totalB = 0;
    contadorB = 0;
    totalC = 0;
    contadorC = 0;

    do{
        printf("Drogeria CentOS!\n");
        printf("Nombre del producto vendido: \n");
        scanf("%s",&producto);
        printf("Ingrese la categoria: A, B, C\n");
        scanf("%s",&categoria);
        if(categoria == 'a' || categoria == 'A'){
            do{
                printf("Precios categoria A: 2000 a 80000 \n");
                printf("Ingrese el precio de venta: \n");
                scanf("%i", &precio);
                i= 0;
                if(precio >= 2000 && precio <= 80000 ){
                    printf("Producto agregado Correctamente! \n");
                    totalA = precio + totalA;
                    contadorA = contadorA + 1;
                }else{
                    printf("Precio fuera de rango, intente nuevamente! \n");
                    i++;
                }
           }while(i>=1);
        }
        else if(categoria == 'b' || categoria == 'B'){
            do{
                printf("Precios categoria B: 8000 a 50000 \n");
                printf("Ingrese el precio de venta: \n");
                scanf("%i", &precio);
                i= 0;
                if(precio >= 8000 && precio <= 50000 ){
                    printf("Producto agregado Correctamente! \n");
                    totalB = precio + totalB;
                    contadorB = contadorB + 1;
                }else{
                    printf("Precio fuera de rando, intente nuevamente! \n");
                    i++;
                }
           }while(i>=1);
        }
        else if(categoria == 'c' || categoria == 'C'){
            do{
                i= 0;
                printf("Precios categoria C: 100000 a 200000  \n");
                printf("Ingrese el precio de venta: \n");
                scanf("%i", &precio);

                if(precio >= 100000 && precio <= 200000 ){
                    printf("Producto agregado Correctamente! \n");
                    totalC = precio + totalC;
                    contadorC = contadorC + 1;
                }else{
                    printf("Precio fuera de rango, intente nuevamente! \n");
                    i++;
                }
           }while(i>=1);
        }else{
            printf("Error en la categoria,intente nuevamente! \n");
        }
        fflush(stdin);
        printf("Presione 'Enter' para ver resultados. \n");
        printf("Presione 's' para agregar otro producto. \n");
        scanf("%c",&opcion);

    }while(opcion == 's' || opcion == 'S');

    printf("Cantidad de productos categoria A: %i\n", contadorA);
    printf("Total de venta A: %i\n", totalA);
    printf("Cantidad de productos categoria B: %i\n", contadorB);
    printf("Total de venta B: %i\n", totalB);
    printf("Cantidad de productos categoria C: %i\n", contadorC);
    printf("Total de venta C: %i\n", totalC);
    totalCantidad= contadorA + contadorB + contadorC;
    printf("Cantidad total de productos vendidos: %i\n", totalCantidad);
    totalglobal= totalA + totalB + totalC;
    printf("Total global de ventas: %i\n", totalglobal);

    printf("\n\nPresione una tecla para continuar...\n");
    return 0;

}

void ejercicio3(){

    int cantidad;
    int i;
    printf("ELECCIONES RECTOR UNIVERSIDAD TUX \n");
    printf("Digite la cantidad de votantes \n");
    scanf("%d", &cantidad);
    char nombre [cantidad][35] ;
    int  identificacion [cantidad];
    char rol [cantidad][15];
    int voto[cantidad];
    int votoCandidato1=0;
    int votoCandidato2=0;
    int votoCandidato3=0;
     for (i=0; i<cantidad; i++){
            printf("Registro votante numero: %d\n",i+1);
            printf("Digite el nombre del votante: \n");
            scanf("%s",&nombre[i]);
            fflush(stdin);
            identificacion[i]=validacion();
            printf("\nDigite el rol dentro de la Univerdidad: (Estudiante, Docente, Administrativo)\n");
            scanf("%s]",&rol[i]);

            voto[i]=votar();
                if(voto[i] == 1){
                    votoCandidato1 = votoCandidato1 + 1;
                }
                else if(voto[i] == 2){
                    votoCandidato2 = votoCandidato2 + 1;
                }
                else{
                    votoCandidato3 = votoCandidato3 + 1;
                }
            }
     datos(nombre , identificacion, rol, voto, cantidad, i, votoCandidato1, votoCandidato2, votoCandidato3);
     return 0;

}
int validacion(){
     int digito;
    do
    {
        printf("Numero de identificacion: \n");
        scanf("%d",&digito);
    }while (digito<0);

    return digito;
}

int votar(){
   int digito;
    do
    {
        printf("\nDigite el numero del candidato rector por el que votara: (1, 2, 3)\n");
        scanf("%d",&digito);
    }while ((digito<=0) || (digito>=4));
    printf("Se ha registrado correctamente!\n");
    return digito;
}
void datos(char nombre [][35], int  identificacion [], char rol [][15], int  voto[], int cantidad, int i, int votoCandidato1, int votoCandidato2, int votoCandidato3 ){

            printf("\n Condolidado de votacion\n");
            for (i=0; i<cantidad; i++){
            printf("Nombre del votante: %s\n",nombre[i]);
            printf("Numero de identificacion: %d\n",identificacion[i]);
            printf("Rol que efectua: %s\n",rol[i]);
            printf("voto: %d\n",voto[i]);

     }
    printf("Cantidad de votos por candidato:\n");
    printf("Votos por candidato numero 1: %i\n", votoCandidato1);
    printf("Votos por candidato numero 2: %i\n", votoCandidato2);
    printf("Votos por candidato numero 3: %i\n", votoCandidato3);
    getchar();
    printf("\n\nPresione una tecla para continuar...\n");
}
